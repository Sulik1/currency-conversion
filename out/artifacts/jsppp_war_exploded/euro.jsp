<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %><%--
  Created by IntelliJ IDEA.
  User: DS
  Date: 24.03.2019
  Time: 11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
</head>
<body style="background-color: antiquewhite">
<h1 style="text-align: center;">
    Kalkulator walut
</h1>
<div style="margin: auto;width: 30%;background-color: burlywood;border: coral 5px solid;height: 400px; border-radius: 40px;">
    <form method="get" action="euro.jsp " style="color: white; text-align: center;padding: 20px">
        Kwota :
        <br>
        <input name="kwota" type="number" required style="width: 25%">
        <br><br><br><br>
        Waluta :
        <br>
        <select name="walutaFrom">
            <option>Eur</option>
            <option>Usd</option>
            <option>Pln</option>
            <option>Chf</option>
        </select>
        <br>
        Waluta na jaką chcesz przeliczyć kwotę:
        <br>
        <select name="walutaTo">
            <option>Eur</option>
            <option>Usd</option>
            <option>Pln</option>
            <option>Chf</option>
        </select>
        <br><br>
        <input class="btn btn-outline-danger" type="submit" value="Przelicz">
    </form>
</div>
<br>
<div style="margin: auto;width: 60%;background-color: burlywood;border: coral 5px solid;height: 150px; text-align: center; font-size: large; border-radius: 40px;">
    <%
        String kwota = request.getParameter("kwota");
        String walutaFrom = request.getParameter("walutaFrom");
        String walutaTo = request.getParameter("walutaTo");
        Map<String, Double> map = new HashMap<String, Double>();
        map.put("Eur", 4.2);
        map.put("Usd", 3.8);
        map.put("Pln", 1.0);
        map.put("Chf", 3.5);

        boolean exist = true;
        if ((kwota == null || kwota.equals("")) || (walutaFrom == null||walutaFrom.equals("")) || (walutaTo == null||walutaTo.equals(""))) {
            exist = false;
        }

        if (exist) {
            double kwotaa = Integer.parseInt(kwota);
            double walutaFroom = map.get(walutaFrom);
            double walutaToo = map.get(walutaTo);
            double result = kwotaa * walutaFroom / walutaToo;
            if (kwotaa < 0) {
                out.write("Brak Parameterów");
            } else {

    %>
    <h1><%=result%>
    </h1>
    <%
            }
        } else {
            out.write("Brak Parameterów");
        }


    %>

</div>


</body>
</html>

